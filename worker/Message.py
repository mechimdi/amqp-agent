import pika
import sys
import logging
import datetime
import time
import json

from enum import Enum

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
_log = logging.getLogger(__name__)

class ExchangeType:
    FANOUT='fanout'
    DIRECT='direct'
    TOPIC='topic'

class DeliveryMode:
    PERSISTENT=2

class Envelope(object):
    def __init__(self,data,*args, **kwargs):
        self.data = data
        self.msg_type = kwargs['msg_type'] if 'msg_type' in kwargs else 'basic'
        self.msg_type = kwargs['sender'] if 'sender' in kwargs else 'anonymous'
        

    def __call__(self):
        return json.dumps({
            'date': str(datetime.datetime.now()),
            'type': self.msg_type,
            'msg':  self.data,
            'sender': self.sender,
            'status': 200
        })

class AMQP(object):  
    
    def __init__(self, host, *args, **kwargs):
        #self.is_durable = kwargs['is_durable'] if 'is_durable' in kwargs else False
        self._exchange_type = kwargs['exchange_type'] if 'exchange_type' in kwargs else False
        self._queue_name = kwargs['queue_name'] if 'queue_name' in kwargs else False

        if self._queue_name:
            self._exchange_name = ''
        else:
            self._exchange_name = kwargs['exchange_name'] if 'exchange_name' in kwargs else False

        self._conn = pika.BlockingConnection(pika.ConnectionParameters(host=host))
        self._channel = self._conn.channel()


    def declare_queue(self, name):
        self._queue_name = name
        self._channel.queue_declare(queue=self._queue_name,
                                    durable=True)

    def declare_exchange(self):
        self._channel.exchange_declare(exchange=self._exchange_name,
                                       exchange_type=self._exchange_type)

    @property
    def exchange_name(self):
        return self._exchange_name
    @property
    def queue_name(self):
        return self._queue_name


    def publish(self, key, body):
        _prop = None
        _log.info(self._exchange_name)
        if self._exchange_name == '' or not self._exchange_name:
            _prop = pika.BasicProperties(delivery_mode=2)
        _log.info(_prop)
        self._channel.basic_publish(exchange=self._exchange_name,
                                        routing_key=key,
                                        body=body,
                                        properties=_prop)

    def close(self):
        self._conn.close()

## Message Workers ##

class MessageTask(AMQP):
    def __init__(self, host, *args, **kwargs):
        super(MessageTask, self).__init__(host, *args, **kwargs)
        self.declare_queue(self._queue_name)

class MessageWorker(AMQP):
    def __init__(self, host, *args, **kwargs):
        super(MessageWorker, self).__init__(host, *args, **kwargs)
        self._prefetch_count = kwargs['prefetch_count'] if 'prefetch_count' in kwargs else 1
        self._channel.queue_declare(queue=self._queue_name,
                                    durable=True)
        self._channel.basic_qos(prefetch_count=self._prefetch_count)


    def consume(self, callback):
        self._channel.basic_consume(callback,
                                    queue=self._queue_name)
        self._channel.start_consuming()


## Message Pub/Sub ##

class MessagePublisher(AMQP):
    def __init__(self, host, *args, **kwargs):
        super(MessagePublisher, self).__init__(host, *args, **kwargs)
        self.declare_exchange()

class MessageSubscriber(AMQP):
    routing_keys = []

    def __init__(self, host, *args, **kwargs):
        super(MessageSubscriber, self).__init__(host, *args, **kwargs)
        self.declare_exchange()
        response = self._channel.queue_declare(exclusive=True)
        self._queue_name = response.method.queue

    def add_topic(self, key):
        self.routing_keys.append(key)

    def consume(self, callback):
        _log.info(self.routing_keys)
        if len(self.routing_keys) < 1:
            self.routing_keys.append(None)

        for key in self.routing_keys:
            _log.info(key)
            self._channel.queue_bind(exchange=self._exchange_name,
                                     queue=self._queue_name,
                                     routing_key=key)
        
        self._channel.basic_consume(callback,
                                    queue=self._queue_name,
                                    no_ack=True)

        self._channel.start_consuming()


class MessageCallback(object):
    def __init__(self):
        pass

    def __call__(self, ch, method, properties, body):
        print(" [x] %r" % body)

def basic_callback(ch, method, properties, body):
    print(" [x] %r" % body)

def worker_callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    time.sleep(body.count(b'.'))
    print(" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)


if __name__ == '__main__':
    cb = MessageCallback()

    if sys.argv[1] == 'pub':
        pass
    elif sys.argv[1] == 'sub':
        pass

    # === Topic ===
    #             /------ error ------>[ | | | | ]  --> [C1]
    #   [P]-->[X]<
    #             \-------- info ----->[ | | | | ]  --> [Cn]
    #              \------ error-----/ 
    #               \-- warning-----/ 
    elif sys.argv[1] == 'direct-pub':
        pub_topic = MessagePublisher('127.0.0.1', exchange_name='direct_log', exchange_type=ExchangeType.DIRECT)
        pub_topic.publish(sys.argv[2], ' '.join(sys.argv[3:]))
        pub_topic.close() 
    elif sys.argv[1] == 'direct-sub': 
        sub_topic = MessageSubscriber('127.0.0.1', exchange_name='direct_log', exchange_type=ExchangeType.DIRECT)
        for topic in sys.argv[2:]:
            sub_topic.add_topic(topic)
        sub_topic.consume(basic_callback)
 
    # === Fanout ===
    #           /----->[ | | | | ] Q1
    # [P]-->[X]<
    #           \----->[ | | | | ] Qn
    elif sys.argv[1] == 'fanout-pub':
        pub_topic = MessagePublisher('127.0.0.1', exchange_name='fanout_logs', exchange_type=ExchangeType.FANOUT)
        pub_topic.publish('', ' '.join(sys.argv[2:]))
        pub_topic.close() 
    elif sys.argv[1] == 'fanout-sub': 
        sub_topic = MessageSubscriber('127.0.0.1', exchange_name='fanout_logs', exchange_type=ExchangeType.FANOUT)
        sub_topic.consume(basic_callback)
 
    # === Topic ===
    #             /-- *.orange.* --->[ | | | | ] Q1 --> [C1]
    #   [P]-->[X]<
    #             \--- *.*.rabbit -->[ | | | | ] Q2 --> [Cn]
    #              \--- lazy.#-----/ 
    elif sys.argv[1] == 'topic-pub':
        pub_topic = MessagePublisher('127.0.0.1', exchange_name='topic_log', exchange_type=ExchangeType.TOPIC)
        pub_topic.publish(sys.argv[2], sys.argv[3])
        pub_topic.close()
    elif sys.argv[1] == 'topic-sub':
        sub_topic = MessageSubscriber('127.0.0.1', exchange_name='topic_log', exchange_type=ExchangeType.TOPIC)
        for topic in sys.argv[2:]:
            sub_topic.add_topic(topic)
        sub_topic.consume(basic_callback)
        
    # === Work Queue ==
    #                        /-----> C1
    # [P]------>[ | | | |]--<
    #                        \-----> C2
    elif sys.argv[1] == 'msg-task':
        task = MessageTask('127.0.0.1',queue_name='task_queue')
        msg = Envelope(' '.join(sys.argv[2:]), msg_type="configuration")
        task.publish('task_queue',msg() or "Empty Message")
        #task.publish('task_queue',' '.join(sys.argv[2:]) or "Empty Message")
        task.close()
    elif sys.argv[1] == 'msg-work':
        worker = MessageWorker('127.0.0.1', queue_name='task_queue', prefetch_count=2)
        worker.consume(worker_callback)
