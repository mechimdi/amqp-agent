#!/usr/bin/env python
import sys
import pika

AMQP_DELIVERY_MODE={
    'persistent':2        
}

connection = pika.BlockingConnection(pika.ConnectionParameters(host='127.0.0.1'))
channel = connection.channel()


channel.queue_declare(queue='task_queue', durable=True)

message = ' '.join(sys.argv[1:]) or "Improper Request"

channel.basic_publish(exchange='',
                      routing_key='task_queue',
                      body=message,
                      properties=pika.BasicProperties(
                        delivery_mode = AMQP_DELIVERY_MODE['persistent']    
                      ))

print(" [x] Sent %r" % message)
connection.close()
